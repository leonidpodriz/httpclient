# HttpClient

В файле `httplient.py` описано два класса: `URL` и `HttpClient`.

### URL
Этот класс решает проблему разбиения адреса на протокол (`http` / `https`), хост и путь.

### HttpClient
Этот класс предназначен для совершения запросов. В класс передается адресс, который разбивается на необходимые 
компоненты с помощью первого класса (`URL`).

### Примеры
Вы можете более детально рассмотеть исходный код обеих классов и попробовать запустить файл `examples.py`, 
чтобы увидеть, как это работает.
```shell script
python3 examples.py
```
Так же в исхожном коде достаточно коментариев, что помогут вам понять в чем суть конкретной обрасти кода.
Если у вас будут вопросы, мы их обязательно рассмотрим на занятии.

## Дополнительно
Обратите внимание, что этот проект мы будем дописывать вместе, разбирая новые темы с курса Python Advanced.

# Успехов!
