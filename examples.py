from httpclient import URL, HttpClient

print(' URL parser examples '.center(50, "="))

url = URL('https://www.google.com/search?q=URL')

print(url)
print("Protocol:", url.protocol)
print("Host:", url.host)
print("Path:", url.path)
print()

url = URL('www.google.com/search?q=URL')

print(url)
print("Protocol:", url.protocol)
print("Host:", url.host)
print("Path:", url.path)
print()

url = URL('www.google.com')

print(url)
print("Protocol:", url.protocol)
print("Host:", url.host)
print("Path:", url.path)
print()

url = URL('google.com/search?q=URL')

print(url)
print("Protocol:", url.protocol)
print("Host:", url.host)
print("Path:", url.path)
print()

print(' HTTPClient examples '.center(50, "="))
http_request = HttpClient('example.com')
print(f'Host:', http_request.url.host)
print(f'({http_request.status_code:3}) Status:', http_request.status_message)
