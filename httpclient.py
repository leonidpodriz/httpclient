"""
Task:
Создайте HTTP клиента, который будет принимать URL ресурса,
тип метода и словарь в качестве передаваемых данных (опциональный).
Выполнять запрос с полученным методом на полученный ресурс,
передавая данные соответствующим методом, и печатать на консоль статус код, заголовки и тело ответа.
"""

import socket


class URL:
    """
    Class to parse URL (Ex: https://www.google.com/search?q=URL) to host, path and protocol
    """
    protocol = None
    host = None
    path = None

    def __init__(self, url):
        self.url = url
        self._parse()  # Init the parse

    def _parse(self):
        self.protocol = self.parse_protocol(self.url)
        self.host, self.path = self.parse_host_and_path(self.url, self.protocol)

    @staticmethod
    def parse_protocol(url):
        parsed_protocol = url.split("://")  # Split https://www.google.com/ to ["https", "www.google.com/"]

        if len(parsed_protocol) > 1:
            return parsed_protocol[0]

    @staticmethod
    def parse_host_and_path(url: str, protocol=None):
        path = '/'
        if protocol:
            url = url.replace(protocol + "://", "")  # Replace protocol (Ex: "http" + "://") with empty string

        parsed_url = url.split('/')
        host = parsed_url[0]
        if len(parsed_url) > 0:
            path = path + '/'.join(parsed_url[1:])  # Join first "/" plus path like 'search?q=CoolThatYouReadIt!'

        return host, path

    def __str__(self):
        return self.url


class HttpClient:
    """
    Make http request to some host, using method (GET by default), data and port.
    HttpClient use socket lib.
    """
    REQUEST_HEADERS = [
        '{method} {path} HTTP/1.0',
        'Host: {host}',
        'Accept: text/html',
    ]

    def __init__(self, url, method="GET", data=None, port=80):
        self.url = URL(url)  # By this we got URL parser. It looks like a host, protocol and path.
        self.method = method
        self.port = port

        self._parse_data(data)
        self._process_connect()
        self._parse_response()

    def _process_connect(self):
        connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        connection.connect((self.url.host, self.port))
        connection.send(self._get_payload().encode())
        received_data = b''

        while True:
            data = connection.recv(1024)
            if not data:  # While have some data continue
                break
            received_data += data

        connection.close()

        self.data = received_data.decode()  # save data in object to continue parsing

    def _parse_response(self):
        data = self.data.split('\n')
        headers = {}
        protocol_version, status_code, status_message = data.pop(0).split(" ", 2)

        while True:
            current_line = data.pop(0).strip()  # Get one by one line and remove this line in array

            if current_line == '':
                break

            key, value = current_line.split(":", 1)
            headers[key.strip()] = value.strip()  # use strip to remove spacing

        self.headers = headers
        self.protocol_version = protocol_version
        self.status_code = status_code
        self.status_message = status_message
        self.text = "\n".join(data)

    def _parse_data(self, data):  # See more: https://ru.wikipedia.org/wiki/POST_(HTTP)
        if not data:
            return None

        result = []
        for key, value in data.items():
            result.append(f"{key}={value}")

        self.data = "&".join(result)

        if self.method == "GET":
            self.url.path += self.data

    def _get_payload(self):
        if self.method != "GET" and self.data:
            self.REQUEST_HEADERS.append(f"content-length: {len(self.data.encode())}")

        self.REQUEST_HEADERS.append(f"\n")  # to split body and headers
        headers_no_data = '\n'.join(self.REQUEST_HEADERS)
        self.request_headers = headers_no_data.format(
            host=self.url.host,
            path=self.url.path,
            method=self.method
        )

        payload = self.request_headers

        if self.method != "GET" and self.data:
            payload += self.data

        return payload
